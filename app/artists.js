const path = require("path");
const express = require("express");
const Artist = require("../models/Artist");
const multer = require("multer");
const { nanoid } = require("nanoid");
const config = require("../config");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  },
});

const upload = multer({ storage });

const router = express.Router();

router.get("/", async (req, res) => {
  try {
    const artist = await Artist.find();
    res.send(artist);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post("/", upload.single("image"), async (req, res) => {
  const artistData = req.body;

  try {
    if (req.file) {
      artistData.image = req.file.filename;
    }

    const artist = new Artist(artistData);
    
    await artist.save();
    res.send(artist);
  } catch (e) {
    res.status(400).send(e);
  }
});

module.exports = router;
