const mongoose = require("mongoose");

const AlbumSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      unique: true,
    },
    date: String,
    image: String,
    artist: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Artist",
      required: true,
    },
  },
  {
    versionKey: false,
  }
);

const Album = mongoose.model("Album", AlbumSchema);
module.exports = Album;
